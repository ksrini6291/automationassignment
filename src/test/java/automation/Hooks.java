package automation;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.URL;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import io.cucumber.java.After;
import io.cucumber.java.Before;


public class Hooks extends BaseStep {

    private BaseStep base;

    public Hooks(BaseStep base)
    {
        this.base = base;

    }

    @Before
    public void Initialize() throws MalformedURLException, IOException
    {
        System.out.println("Initializing webdriver...");
        String currentDir = System.getProperty("user.dir");
        // System.setProperty("webdriver.chrome.driver",currentDir+"/chromedriver.exe");
        // base.driver = new ChromeDriver();
        ArrayList<String> props = readProperties();
        // Commented below lines as they are used to run webdriver in gitlab docker images
        String node = "http://selenium__standalone-chrome:4444/wd/hub/";        
        DesiredCapabilities dc = DesiredCapabilities.chrome();
        dc.setBrowserName("chrome");
        // dc.setVersion("81.0");
        base.driver = new RemoteWebDriver(new URL(node), dc);
        base.driver.get(props.get(0));
        base.driver.manage().window().maximize();
        base.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void Teardown()
    {
        base.driver.quit();
        System.out.println("cleanup completed");
    }

    public ArrayList<String> readProperties() throws IOException
    {
        InputStream input = new FileInputStream(System.getProperty("user.dir")+"/TestVariables.properties");
        Properties properties = new Properties();
        properties.load(input);
        ArrayList<String> props = new ArrayList<>();
        props.add(properties.get("URL").toString());
        props.add(properties.getProperty("Browser"));
        return props;
    }
}
